'''
Snippets to show how to implement a switch/case-like statement in Python.
'''

import operator
ops = { "+": operator.add, "-": operator.sub, "*": operator.mul }

def func(var1, var2, var3):
    result = ops[var2](var1,var3)
    print(result)

# if-elif-else statement
def foo_1(var,x):
    if var == 'a':
        func(x, '*', 3)
    elif var == 'b':
        func(x, '+', 5)
    elif var == 'c':
        func(x, '-', 7)		

# Re-implement in switch-case-like statement. It's equivalent to foo_1.
def foo_2(var,x):
    {   'a': lambda x: func(x, '*', 3),
        'b': lambda x: func(x, '+', 5),
        'c': lambda x: func(x, '-', 7), 
    }[var](x)
				
foo_2('a', 2)

